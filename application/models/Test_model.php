<?php 
   Class Test_model extends CI_Model {

      function __construct() { 
         parent::__construct(); 
      } 
   
      public function insert($data) { 
         if ($this->db->insert("user", $data)) { 
            return true; 
         } 
      } 
   
      public function delete($id) { 
         if ($this->db->delete("user", "userId = ".$id)) { 
            return true; 
         } 
      } 
   
      public function update($data,$name) { 
         $this->db->set($data); 
         $this->db->where("name", $name); 
         $this->db->update("user", $data); 
      } 
   } 
?>